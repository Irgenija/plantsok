from apps import app, db
from apps.users.models import User
from apps.plants.models import Plant

if __name__ == "__main__":
    app.run(debug=True)


@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Plant': Plant}
