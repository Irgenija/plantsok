# -*- coding: utf-8 -*-

from flask import render_template, flash, redirect, url_for, request
from apps import app, db
from apps.forms import LoginForm, RegistrationForm, EditProfileForm, \
    AddPlantsForm, EditPlantsForm
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.urls import url_parse

from apps.users.models import User
from apps.plants.models import Plant


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    form = AddPlantsForm()
    if form.validate_on_submit():
        plant = Plant(
            type=form.type.data,
            name=form.name.data,
            size=form.size.data,
            purchase_date=form.purchase_date.data,
            seller=form.seller.data,
            user_id=current_user.id
        )
        db.session.add(plant)
        db.session.commit()
        flash('Your plant was added')
        return redirect(url_for('index'))
    return render_template('index.html', title='Home', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/user_profile/')
@login_required
def user_profile():
    page = request.args.get('page', 1, type=int)
    plants = Plant.query.filter_by(user_id=current_user.id).paginate(
        page, int(app.config['PLANTS_PER_PAGE']), False)
    next_url = url_for(
        'user_profile', page=plants.next_num,
        ) if plants.has_next else None
    prev_url = url_for(
        'user_profile', page=plants.prev_num,
        ) if plants.has_prev else None
    return render_template('user.html',
                           title=current_user.username,
                           user=current_user,
                           plants=plants.items,
                           next_url=next_url,
                           prev_url=prev_url
                           )


@app.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
    return render_template('edit_profile.html', title='Edit Profile',
                           form=form)


@app.route('/delete_plant/<int:id>', methods=['POST'])
@login_required
def delete_plant(id):
    plant = Plant.query.get_or_404(id)
    db.session.delete(plant)
    db.session.commit()
    flash('Your changes have been saved.')
    return redirect(url_for('user_profile'))


@app.route('/edit_plant/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_plant(id):
    plant = Plant.query.filter_by(id=id).first_or_404()
    form = EditPlantsForm(obj=plant)
    if form.validate_on_submit():
        plant.type = form.type.data
        plant.name = form.name.data
        plant.size = form.size.data
        plant.purchase_date = form.purchase_date.data
        plant.seller = form.seller.data
        db.session.commit()
        flash('Your plant was edit')
        return redirect(url_for('edit_plant', id=id))
    return render_template('edit_plant.html', title='Edit Plant', form=form)
