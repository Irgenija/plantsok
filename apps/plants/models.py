from datetime import datetime

from sqlalchemy import Column, String, DateTime, ForeignKey, Integer

from apps.core.models import DateTimeModel


class Plant(DateTimeModel):
    """ Plant model. """

    __tablename__ = 'plants'

    type = Column(String(64))
    name = Column(String(128))
    size = Column(String(64))
    purchase_date = Column(DateTime, default=datetime.utcnow())
    seller = Column(String(128), nullable=True)
    user_id = Column(Integer, ForeignKey('users.id'))
