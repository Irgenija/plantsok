from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, \
    DateTimeField, SubmitField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from apps.users.models import User
from apps import db


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, field):
        user_exists = db.session.query(
            User.query.filter(
                User.username == field.data,
            ).exists(),
        ).scalar()
        if user_exists:
            raise ValidationError('Please use a different username.')

    def validate_email(self, field):
        user_exists = db.session.query(
            User.query.filter(
                User.email == field.data,
            ).exists(),
        ).scalar()
        if user_exists:
            raise ValidationError('Please use a different email address.')


class EditProfileForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    submit = SubmitField('Submit')

    def __init__(self, original_username, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.original_username = original_username

    def validate_username(self, field):
        if field.data != self.original_username:
            user_exists = db.session.query(
                User.query.filter(
                    User.username == field.data,
                ).exists(),
            ).scalar()
            if user_exists:
                raise ValidationError('Please use a different username.')


class AddPlantsForm(FlaskForm):
    type = StringField('Type', validators=[DataRequired()])
    name = StringField('Name', validators=[DataRequired()])
    size = StringField('Size', validators=[DataRequired()])
    purchase_date = DateTimeField('Purchase_date', format='%Y-%m-%d',
                                  validators=[DataRequired()])
    seller = StringField('Seller', validators=[DataRequired()])
    submit = SubmitField('Submit')


class EditPlantsForm(AddPlantsForm):
    pass
