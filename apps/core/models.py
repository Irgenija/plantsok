from datetime import datetime
from apps import db


class BaseModel(db.Model):
    """ Base core model. """

    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)


class DateTimeModel(BaseModel):
    """ Model with created_at and updated_at fields. """

    __abstract__ = True
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow())
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow())
